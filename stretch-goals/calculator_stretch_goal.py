import math


def calculator(a, b, operator):
    # ==============
    # Your code here
    if operator == "+":
        return format(a + b, "b")
    elif operator == "-":
        return format(a - b, "b")
    elif operator == "*":
        return format(a * b, "b")
    elif operator == "/":
        # had to round this down since binary representations of floats get complicated.
        return format(math.floor(a / b), "b")
    else:
        raise Exception("Unsupported operator")
    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
