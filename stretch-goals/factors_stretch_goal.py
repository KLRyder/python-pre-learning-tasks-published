import math


def factors(number):
    # ==============
    # Your code here
    # So this is a pretty slow operation when the input gets large and it is using sets to remove duplicate values which
    # isn't great.

    found_factors = set()
    for i in range(2, math.floor(number / 2), 1):
        if number % i == 0:
            found_factors.add(i)
            found_factors.add(number // i)

    if len(found_factors) == 0:
        return str(number) + " is a prime number"
    else:
        # sorting the list isn't *technically* needed here since my implementation of python automatically sorts sets
        # but sets aren't actually ordered in the language spec.
        return sorted(list(found_factors))
    # ==============


print(factors(15))  # Should print [3, 5] to the console
print(factors(12))  # Should print [2, 3, 4, 6] to the console
print(factors(13))  # Should print “13 is a prime number”
