def vowel_swapper(string):
    # ==============
    # Your code here

    # not sure that im happy with this implementation. Its extendable pretty easily due to the dictionary, but having to
    # check it for each letter feels a little inefficient.
    replacments = {
        "a": "4",
        "A": "4",
        "e": "3",
        "E": "3",
        "i": "!",
        "I": "!",
        "o": "ooo",
        "O": "000",
        "u": "|_|",
        "U": "|_|",
    }

    return_string = [replacments.get(char, char) for char in string]

    return "".join(return_string)
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console